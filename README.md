# Expleurer

This project is a simplified file explorer for Windows.

## Requirements

- Windows 10
- Python 3.10.0

## Install

Clone this repository, then execute the following commands:

```
python3 -m venv venv
.\venv\Scripts\activate
pip install -r requirements 
python -m expleurer
```

Or use your favorite IDE to build venv and execute the mainwindows.py file.

## Custom feature

Since release 0.2.0, you can use the `--custom-style` argument to load the QSS style store in `expleurer/ui/qss/`
directory. You are free to custom this file.

## Licence

- GPL3 (check LICENSE file)