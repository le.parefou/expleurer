"""Module according to file modify manipulation"""
from pathlib import Path
from shutil import copytree, rmtree

from winshell import shortcut


def create_file(filename: str, path: Path) -> None:
    """
    Create a file with the name filename in the path

    :param filename: The name of the file that will be created
    :type filename: str
    :param path: The path where the file will be created
    :type path: Path
    :raises FileExistsError: This error is raise when the filename already exists in the path
    :raises ValueError: This error is raise when the filename is not set (is empty)
    """
    if not filename:
        raise ValueError('Le nom de fichier ne peut pas être vide.')
    try:
        (path / filename).touch(exist_ok=False)
    except FileExistsError as fee:
        raise FileExistsError('Le nom de fichier existe déjà : renommez-le.') from fee


def create_directory(directory_name: str, path: Path) -> None:
    """
    Create a directory with the name directory_name in the path

    :param directory_name: The name of the directory that will be created
    :type directory_name: str
    :param path: The path where the directory will be created
    :type path: Path
    :raise FileExistsError: This error is raise when the directory name already exists in the path
    :raises ValueError: This error is raise when the directory name is not set (is empty)
    """
    if not directory_name:
        raise ValueError('Le répertoire ne peut pas être vide.')
    try:
        (path / directory_name).mkdir(exist_ok=False)
    except FileExistsError as fee:
        raise FileExistsError('Le nom du répertoire existe déjà : renommez-le.') from fee


def create_shortcut(shortcut_name: str, source: str, path: Path) -> None:
    """
    Create a shortcut with the name shortcut_name in the path.
    This shortcut points to the source.

    :param shortcut_name: The name of the shortcut that will be created
    :type shortcut_name: str
    :param source: The name given to the file / folder where the shortcut points. This is the absolute path.
    :type source: str
    :param path: The path where the shortcut will be created
    :raises ValueError: This error is raise when the shortcut name doesn't have .lnk suffix
                        This error is raise when the shortcut name is not set (is empty)
                        This error is raise when the target name is not set (is empty)
    :raise FileNotFoundError: This error is raise when the shortcut target doesn't exist
    """
    if not source:
        raise ValueError('La cible du raccourci ne peut pas être vide.')
    if not shortcut_name:
        raise ValueError('Le nom du raccourci ne peut pas être vide.')
    if (path / shortcut_name).suffix != '.lnk':
        raise ValueError('Le nom du raccourci doit avoir lnk comme suffixe.')
    if not Path(source).exists():
        raise FileNotFoundError('Le raccourci cible n\'existe pas.')
    the_shortcut = shortcut(source)
    the_shortcut.write(str((path / shortcut_name).resolve()))


def rename_file(initial_name: str, new_name: str, path: Path) -> None:
    """
    Rename the file with initial_name to new_name in the path.

    :param initial_name: The current name of the file.
    :type initial_name: str
    :param new_name: The new name of the file.
    :type new_name: str
    :param path: The path where the current file is
    :type path: Path
    :raise FileExistsError: This error is raise when the file already exists in the path
    """
    try:
        (path / initial_name).rename(path / new_name)
    except FileExistsError as fee:
        raise FileExistsError('Filename already exists : rename it.') from fee


def rename_directory(initial_name: str, new_name: str, path: Path) -> None:
    """
    Rename the directory with initial_name to new_name in the path.

    :param initial_name: The current name of the directory.
    :type initial_name: str
    :param new_name: The new name of the directory.
    :type new_name: str
    :param path: The path where the current directory is
    :type path: Path
    :raise FileExistsError: This error is raise when the directory already exists in the path
    """
    try:
        rename_file(initial_name, new_name, path)
    except FileExistsError as fee:
        raise FileExistsError('Directory name already exists : rename it.') from fee


def rename_shortcut(shortcut_name: str, new_name: str, path: Path) -> None:
    """
    Rename the shortcut with initial_name to new_name in the path.

    :param shortcut_name: The current name of the shortcut.
    :type shortcut_name: str
    :param new_name: The new name of the shortcut.
    :type new_name: str
    :param path: The path where the current shortcut is
    :type path: Path
    :raise FileExistsError: This error is raise when the shortcut already exists in the path
    :raise ValueError: This error is raise when the shortcut name doesn't have .lnk suffix
    """
    if (path / new_name).suffix != '.lnk':
        raise ValueError('The new shortcut name need to have .lnk in suffix')
    rename_file(shortcut_name, new_name, path)


def delete_file(filename: str, path: Path) -> None:
    """
    Delete a file with the name filename in the path

    :param filename: The name of the file that will be deleted
    :type filename: str
    :param path: The path where the file will be deleted
    :type path: Path
    :raise FileNotFoundError: This error is raise when the file doesn't exist
    :raise NotADirectoryError: This error is raise when the path is a file
    """
    if not path.is_dir():
        raise NotADirectoryError('The path is not a directory')
    try:
        (path / filename).unlink()
    except FileNotFoundError as fnfe:
        raise FileNotFoundError('The file cannot delete : not exist') from fnfe


def delete_directory(directory_name: str, path: Path) -> None:
    """
    Delete a directory with the name directory_name in the path.

    :param directory_name: The name of the directory that will be deleted
    :type directory_name: str
    :param path: The path where the directory will be deleted
    :type path: Path
    :raise FileNotFoundError: This error is raise when the directory doesn't exist
    :raise NotADirectoryError: This error is raise when the directory name is a file or the path is a file
    """
    try:
        if not path.is_dir():
            raise NotADirectoryError('The path is not a directory')
        (path / directory_name).rmdir()
    except FileNotFoundError as fnfe:
        raise FileNotFoundError('The directory cannot delete : not exist') from fnfe
    except NotADirectoryError as nade:
        raise NotADirectoryError('This element cannot be delete : something wrong with directory') from nade
    except OSError:
        rmtree((path / directory_name).resolve())


def delete_shortcut(shortcut_name: str, path: Path) -> None:
    """
    Delete a shortcut with the name shortcut_name in the path

    :param shortcut_name: The name of the shortcut that will be deleted
    :type shortcut_name: str
    :param path: The path where the shortcut will be deleted
    :type path: Path
    :raise FileNotFoundError: This error is raise when the shortcut doesn't exist
    """
    if (path / shortcut_name).suffix != '.lnk':
        raise ValueError('The target name need to have .lnk in suffix')
    delete_file(shortcut_name, path)


def move_file(initial_name: str, target_name: str, path: Path) -> None:
    """
     Move the file with initial_name to new_name in the path.

    :param initial_name: The current name of the file.
    :type initial_name: str
    :param target_name:  The target name of the file. Can be a relative path according to path param.
    :type target_name: str
    :param path: The path where the current file is
    :type path: Path
    :raise FileExistsError: This error is raise when the file already exist
    """
    try:
        (path / initial_name).rename(target_name)
    except FileExistsError as fee:
        raise FileExistsError('You can move this file : already exist') from fee


def move_directory(initial_name: str, target_name: str, path: Path) -> None:
    """
     Move the directory with initial_name to new_name in the path.

    :param initial_name: The current name of the directory.
    :type initial_name: str
    :param target_name:  The target name of the directory. Can be a relative path according to path param.
    :type target_name: str
    :param path: The path where the current directory is
    :type path: Path
    :raise FileExistsError: This error is raise when the directory already exist
    :raise NotADirectoryError: This error is raise when the initial name is a file
    """
    if not (path / initial_name).is_dir():
        raise NotADirectoryError('The path is not a directory')
    move_file(initial_name, target_name, path)


def move_shortcut(initial_name: str, target_name: str, path: Path) -> None:
    """
     Move the shortcut with initial_name to new_name in the path.

    :param initial_name: The current name of the shortcut.
    :type initial_name: str
    :param target_name:  The target name of the shortcut. Can be a relative path according to path param.
    :type target_name: str
    :param path: The path where the current shortcut is
    :type path: Path
    :raise FileExistsError: This error is raise when the shortcut already exist
    """
    if (path / target_name).suffix != '.lnk':
        raise ValueError('The target name need to have .lnk in suffix')
    move_file(initial_name, target_name, path)


def copy_file(initial_file: str, copy_file_name: str, path: Path) -> None:
    """
    Copy the file with initial_file to copy_file_name in the path.

    :param initial_file: The filename that will be copied
    :type initial_file: str
    :param copy_file_name: The name of the copy file. Can be a relative path according to path param.
    :type copy_file_name: str
    :param path: The path where the current file is
    :type path: Path
    """
    (path / copy_file_name).touch()
    content = (path / initial_file).read_bytes()
    (path / copy_file_name).write_bytes(content)


def copy_directory(initial_directory: str, copy_directory_name: str, path: Path) -> None:
    """
    Copy the file with initial_directory to copy_directory_name in the path.

    :param initial_directory: The directory that will be copied
    :type initial_directory: str
    :param copy_directory_name: The name of the copy directory. Can be a relative path according to path param.
    :type copy_directory_name: str
    :param path: The path where the current directory is
    :type path: Path
    """
    copytree((path / initial_directory), (path / copy_directory_name))


def copy_shortcut(shortcut_name: str, copy_shortcut_name: str, path: Path) -> None:
    """
    Copy the file with shortcut_name to copy_shortcut_name in the path.

    :param shortcut_name: The shortcut that will be copied
    :type shortcut_name: str
    :param copy_shortcut_name: The name of the copy shortcut. Can be a relative path according to path param.
    :type copy_shortcut_name: str
    :param path: The path where the current directory is
    :type path: Path
    """
    if (path / copy_shortcut_name).suffix != '.lnk':
        raise ValueError('The copy shortcut name need to have .lnk in suffix')
    copy_file(shortcut_name, copy_shortcut_name, path)
