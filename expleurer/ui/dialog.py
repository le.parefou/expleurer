"""All the dialog call by the GUI"""
from pathlib import Path

from PySide6.QtCore import Signal, QThread
from PySide6.QtWidgets import QDialog, QDialogButtonBox, QLabel, QLineEdit, QVBoxLayout, QHBoxLayout

from expleurer.modify import create_shortcut, create_file, create_directory, rename_file, rename_shortcut, \
    rename_directory
from expleurer.view import search_in_path


class ErrorDialog(QDialog):
    """Dialog Windows when an error occurred"""

    def __init__(self, message: str):
        super().__init__()
        self.setObjectName("error dialog")
        self.setWindowTitle("Une erreur est survenue")
        self.resize(250, 70)

        exit_buttons = QDialogButtonBox.Ok
        dialog_buttons = QDialogButtonBox(exit_buttons)
        dialog_buttons.accepted.connect(self._accepted)
        self.label_message = QLabel(message)

        layout = QVBoxLayout()
        layout.addWidget(self.label_message)
        layout.addWidget(dialog_buttons)

        self.setLayout(layout)

    def _accepted(self):
        self.close()


class FileDialog(QDialog):
    """Dialog Windows when you create a file"""

    def __init__(self, current_directory: Path):
        super().__init__()
        self.setObjectName("Create a file")
        self.setWindowTitle("Nom du fichier")
        self.resize(250, 70)
        self._current_directory = current_directory

        exit_buttons = QDialogButtonBox.Ok | QDialogButtonBox.Cancel
        dialog_buttons = QDialogButtonBox(exit_buttons)
        dialog_buttons.accepted.connect(self._accepted)
        dialog_buttons.rejected.connect(self._canceled)
        message = QLabel("Quel est le nom du nouveau fichier ?")
        self.edit_name = QLineEdit()

        layout = QVBoxLayout()
        layout2 = QVBoxLayout()
        layout2.addWidget(message)
        layout2.addWidget(self.edit_name)

        layout.addLayout(layout2)
        layout.addWidget(dialog_buttons)
        self.setLayout(layout)

    def _accepted(self):
        filename_create = self.edit_name.text().rstrip()

        try:
            create_file(filename_create, self._current_directory)
            self.close()
        except FileExistsError as file_exists_error:
            dialog = ErrorDialog(str(file_exists_error))
            dialog.exec()
        except ValueError as value_error:
            dialog = ErrorDialog(str(value_error))
            dialog.exec()
        except OSError:
            dialog = ErrorDialog("Un caractère spéciale n'est pas autorisé : | / \\ ? * < > \"")
            dialog.exec()
        self.edit_name.setText(filename_create)

    def _canceled(self):
        self.close()


class DirectoryDialog(QDialog):
    """Dialog Windows when you create a directory"""

    def __init__(self, current_directory: Path):
        super().__init__()
        self.setObjectName("Create a directory")
        self.setWindowTitle("Nom du dossier")
        self._current_directory = current_directory
        self.dialog: ErrorDialog

        exit_buttons = QDialogButtonBox.Ok | QDialogButtonBox.Cancel
        dialog_buttons = QDialogButtonBox(exit_buttons)
        dialog_buttons.accepted.connect(self._accepted)
        dialog_buttons.rejected.connect(self._canceled)
        message = QLabel("Quel est le nom du nouveau dossier ?")
        self.edit_name = QLineEdit()

        layout = QVBoxLayout()
        layout2 = QVBoxLayout()
        layout2.addWidget(message)
        layout2.addWidget(self.edit_name)

        layout.addLayout(layout2)
        layout.addWidget(dialog_buttons)
        self.setLayout(layout)

    def _accepted(self):
        filename_create = self.edit_name.text().rstrip()

        try:
            create_directory(filename_create, self._current_directory)
            self.close()
        except FileExistsError as file_exists_error:
            self.dialog = ErrorDialog(str(file_exists_error))
            self.dialog.exec()
        except ValueError as value_error:
            self.dialog = ErrorDialog(str(value_error))
            self.dialog.exec()
        except OSError:
            self.dialog = ErrorDialog("Un caractère spéciale n'est pas autorisé : | / \\ ? * < > \"")
            self.dialog.exec()
        self.edit_name.setText(filename_create)

    def _canceled(self):
        self.close()


class ShortcutDialog(QDialog):
    """Dialog Windows when you create a shortcut"""

    def __init__(self, current_directory: Path):
        super().__init__()
        self.setObjectName("create a shortcut")
        self.setWindowTitle("Nom du raccourci")
        self._current_directory = current_directory
        self.dialog: ErrorDialog

        exit_buttons = QDialogButtonBox.Ok | QDialogButtonBox.Cancel
        dialog_buttons = QDialogButtonBox(exit_buttons)
        dialog_buttons.accepted.connect(self._accepted)
        dialog_buttons.rejected.connect(self._canceled)

        message = QLabel("Quel est le nom du nouveau raccourcis ?")
        label_shortcut_name = QLabel("Nom du raccourcis (ajouter .lnk)")
        label_shortcut_target = QLabel("Cible du raccourci")
        self.edit_name = QLineEdit()
        self.edit_target = QLineEdit()

        hbox = QHBoxLayout()
        hbox.addWidget(label_shortcut_name)
        hbox.addWidget(self.edit_name)
        hbox1 = QHBoxLayout()
        hbox1.addWidget(label_shortcut_target)
        hbox1.addWidget(self.edit_target)

        layout = QVBoxLayout()
        layout.addWidget(message)
        layout.addLayout(hbox)
        layout.addLayout(hbox1)

        layout.addLayout(layout)
        layout.addWidget(dialog_buttons)
        self.setLayout(layout)

    def _accepted(self):
        filename_create = self.edit_name.text().rstrip()
        target_shortcut = self.edit_target.text().rstrip()
        try:
            create_shortcut(filename_create, target_shortcut, self._current_directory)
            self.close()
        except FileNotFoundError as file_not_found_error:
            self.dialog = ErrorDialog(str(file_not_found_error))
            self.dialog.exec()
        except ValueError as value_error:
            self.dialog = ErrorDialog(str(value_error))
            self.dialog.exec()
        self.edit_name.setText(filename_create)

    def _canceled(self):
        self.close()


class WorkerSearchThread(QThread):
    resultready = Signal(list)

    def __init__(self, filename_pattern, current_directory):
        super(WorkerSearchThread, self).__init__()
        self.filename_pattern = filename_pattern
        self._current_directory = current_directory
        self._result = []

    def run(self):
        self._result = search_in_path(self.filename_pattern, str(self._current_directory))
        self.resultready.emit(self._result)


class SearchDialog(QDialog):
    """Dialog Windows when you search a file"""
    pattern = Signal(str)

    def __init__(self, current_directory: Path):
        super().__init__()
        self.setObjectName("search a file")
        self.setWindowTitle("Recherche d'un fichier")
        self._current_directory = current_directory
        self._result = []

        exit_buttons = QDialogButtonBox.Ok | QDialogButtonBox.Cancel
        dialog_buttons = QDialogButtonBox(exit_buttons)
        dialog_buttons.accepted.connect(self._accepted)
        dialog_buttons.rejected.connect(self._canceled)
        message = QLabel("Que recherchez-vous ?")
        self.edit_name = QLineEdit()

        layout = QVBoxLayout()
        layout2 = QVBoxLayout()
        layout2.addWidget(message)
        layout2.addWidget(self.edit_name)

        layout.addLayout(layout2)
        layout.addWidget(dialog_buttons)
        self.setLayout(layout)

    def _handle_result(self, result):
        self._result = result
        self.close()

    def _accepted(self):
        filename_pattern = self.edit_name.text().rstrip()
        self.pattern.emit(filename_pattern)
        self.close()

    def get_result(self) -> list[str]:
        """Get all file matching the search

        :return: all file matching the search
        :rtype: list[str]
        """
        return self._result

    #
    # def get_search(self) -> str:
    #     """Get the pattern search
    #
    #     :return: pattern search
    #     :rtype: str"""
    #     return self.edit_name.text().rstrip()

    def _canceled(self):
        self.close()


class RenameDialog(QDialog):
    """Dialog Windows when you rename a file"""

    def __init__(self, name, current_directory: Path):
        super().__init__()
        self.setObjectName("search a file")
        self.setWindowTitle("Renommer ")
        self._current_directory = current_directory
        self._name = name
        exit_buttons = QDialogButtonBox.Ok | QDialogButtonBox.Cancel
        dialog_buttons = QDialogButtonBox(exit_buttons)
        dialog_buttons.accepted.connect(self._accepted)
        dialog_buttons.rejected.connect(self._canceled)
        message = QLabel("Quel est le nouveau nom du fichier ?")

        if (self._current_directory / name).is_dir():
            message = QLabel("Quel est le nouveau nom du dossier ?")
        elif (self._current_directory / name).suffix == '.lnk':
            message = QLabel("Quel est le nouveau nom du raccourci ?")
        self.edit_name = QLineEdit()
        self.edit_name.setText(name)

        layout = QVBoxLayout()
        layout2 = QVBoxLayout()
        layout2.addWidget(message)
        layout2.addWidget(self.edit_name)

        layout.addLayout(layout2)
        layout.addWidget(dialog_buttons)
        self.setLayout(layout)

    def _accepted(self):
        filename_pattern = self.edit_name.text().rstrip()
        if (self._current_directory / self._name).is_dir():
            rename_directory(self._name, filename_pattern, self._current_directory)
        elif (self._current_directory / self._name).suffix == '.lnk':
            if not filename_pattern.endswith('.lnk'):
                filename_pattern += '.lnk'
            rename_shortcut(self._name, filename_pattern, self._current_directory)
        else:
            rename_file(self._name, filename_pattern, self._current_directory)
        self.close()

    def _canceled(self):
        self.close()
