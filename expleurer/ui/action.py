"""All the action present in the GUI"""
from PySide6.QtCore import QObject
from PySide6.QtGui import QAction


class CreateAFile(QAction):
    """Action to create a file"""

    def __init__(self, parent: QObject | None):
        super().__init__(parent)
        self.setObjectName("actionCreate_a_file")


class CreateADirectory(QAction):
    """Action to create a directory"""

    def __init__(self, parent: QObject | None):
        super().__init__(parent)
        self.setObjectName("actionCreate_a_directory")


class CreateAShortcut(QAction):
    """Action to create a shortcut"""

    def __init__(self, parent: QObject | None):
        super().__init__(parent)
        self.setObjectName("actionCreate_a_shortcut")


class SortByName(QAction):
    """Action to sort by name"""

    def __init__(self, parent: QObject | None):
        super().__init__(parent)
        self.setObjectName("actionBy_name")


class SortByType(QAction):
    """Action to sort by type"""

    def __init__(self, parent: QObject | None):
        super().__init__(parent)
        self.setObjectName("actionBy_type")


class SortByExtension(QAction):
    """Action to sort by extension"""

    def __init__(self, parent: QObject | None):
        super().__init__(parent)
        self.setObjectName("actionBy_extension")


class SortByModificationDate(QAction):
    """Action to sort by modification date"""

    def __init__(self, parent: QObject | None):
        super().__init__(parent)
        self.setObjectName("actionBy_modification_date")


class SimpleDisplay(QAction):
    """Action to display only the name of the file / directory / shortcut"""

    def __init__(self, parent: QObject | None):
        super().__init__(parent)
        self.setObjectName("actionSimple")


class DetailsDisplay(QAction):
    """Action to display the name, type and modification date of the file / directory / shortcut"""

    def __init__(self, parent: QObject | None):
        super().__init__(parent)
        self.setObjectName("actionIn_detail")


class SearchSomething(QAction):
    """This is the search action"""

    def __init__(self, parent: QObject | None):
        super().__init__(parent)
        self.setObjectName("actionSearch")


class MoveSomething(QAction):
    """Action to move something (file, directory, shortcut)"""

    def __init__(self, parent: QObject | None):
        super().__init__(parent)
        self.setObjectName("actionMove")


class CopySomething(QAction):
    """Action to copy something (file, directory, shortcut)"""

    def __init__(self, parent: QObject | None):
        super().__init__(parent)
        self.setObjectName("actionCopy")


class CutSomething(QAction):
    """Action to cut something (file, directory, shortcut)"""

    def __init__(self, parent: QObject | None):
        super().__init__(parent)
        self.setObjectName("actionCut")


class DeleteSomething(QAction):
    """Action to delete something (file, directory, shortcut)"""

    def __init__(self, parent: QObject | None):
        super().__init__(parent)
        self.setObjectName("actionDelete")


class OpenSomething(QAction):
    """Action to open something (file, directory, shortcut)"""

    def __init__(self, parent: QObject | None):
        super().__init__(parent)
        self.setObjectName("actionOpen_by_default")


class RenameSomething(QAction):
    """Action to rename something (file, directory, shortcut)"""

    def __init__(self, parent: QObject | None):
        super().__init__(parent)
        self.setObjectName("actionRename")
