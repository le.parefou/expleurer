"""All the fonts present in the GUI"""
from PySide6.QtGui import QFont


class DefaultFont(QFont):
    """This is the default font of the project"""

    def __init__(self):
        super().__init__()
        self.setFamily("Roboto")
        self.setPointSize(12)
