"""Where the GUI is begin"""
import os
import sys
from datetime import datetime, timezone
from pathlib import Path
from stat import FILE_ATTRIBUTE_SYSTEM, FILE_ATTRIBUTE_NOT_CONTENT_INDEXED, FILE_ATTRIBUTE_READONLY

from PySide6.QtCore import QCoreApplication, QModelIndex, Qt
from PySide6.QtGui import QIcon, QKeyEvent, QAction
from PySide6.QtWidgets import (QMainWindow, QApplication, QHBoxLayout, QWidget, QTreeWidget, QTreeWidgetItem,
                               QTableWidget, QTableWidgetItem, QVBoxLayout, QLineEdit, QHeaderView)
from psutil import disk_partitions

from expleurer.modify import delete_directory, delete_shortcut, delete_file, copy_directory, copy_shortcut, copy_file, \
    move_directory, move_shortcut, move_file
from expleurer.structure.typefile import suffix_to_type
from expleurer.ui.action import (CreateAFile, CreateADirectory, CreateAShortcut, SortByName, SortByType,
                                 SortByExtension,
                                 SortByModificationDate, SimpleDisplay, DetailsDisplay, MoveSomething, CopySomething,
                                 CutSomething, DeleteSomething, OpenSomething, RenameSomething, SearchSomething)
from expleurer.ui.dialog import FileDialog, DirectoryDialog, ShortcutDialog, SearchDialog, RenameDialog, \
    WorkerSearchThread
from expleurer.ui.fonts import DefaultFont
from expleurer.ui.menu import MainMenuBar, FileMenu, SortMenu, DisplayMenu, HelpMenu
from expleurer.view import sort_by_name, sort_by_type, sort_by_extension, sort_by_date


class UiExpleurer(QMainWindow):
    """This is the main Windows"""

    def __init__(self):
        super().__init__()
        self._current_directory = Path().home()
        self._is_display_simple = False
        self._is_copy_active = True
        self._tmp_search = []
        self._tmp_file_move = None
        self._sorted_by = sort_by_name
        self.tree_widget: QTreeWidget
        self.table_widget: QTableWidget
        self.line_edit: QLineEdit

        self._setup_ui()
        self._load_data()
        self._display_details()
        self.show()

    def _setup_ui(self):
        self._main_windows_configuration()
        all_actions = self._actions_configuration()
        self._layout_configuration()
        self._menu_configuration(all_actions)

    def _actions_configuration(self) -> list[QAction]:
        action_cr_er_un_fichier = CreateAFile(self)
        action_cr_er_un_fichier.triggered.connect(self._create_file)
        action_cr_er_un_fichier.setText(QCoreApplication.translate("UiExpleurer", "Cr\u00e9er un fichier", None))
        action_cr_er_un_fichier.setShortcut("CTRL+N")

        action_cr_er_un_dossier = CreateADirectory(self)
        action_cr_er_un_dossier.triggered.connect(self._create_directory)
        action_cr_er_un_dossier.setText(QCoreApplication.translate("UiExpleurer", "Cr\u00e9er un dossier", None))

        action_cr_er_un_raccourci = CreateAShortcut(self)
        action_cr_er_un_raccourci.triggered.connect(self._create_shortcut)
        action_cr_er_un_raccourci.setText(
            QCoreApplication.translate("UiExpleurer", "Cr\u00e9er un raccourci", None))

        action_par_nom = SortByName(self)
        action_par_nom.triggered.connect(self._sort_by_name)
        action_par_nom.setText(QCoreApplication.translate("UiExpleurer", "Par nom", None))
        action_par_type = SortByType(self)
        action_par_type.triggered.connect(self._sort_by_type)
        action_par_type.setText(QCoreApplication.translate("UiExpleurer", "Par type", None))
        action_par_extension = SortByExtension(self)
        action_par_extension.triggered.connect(self._sort_by_extension)
        action_par_extension.setText(QCoreApplication.translate("UiExpleurer", "Par extension", None))
        action_par_date_de_modification = SortByModificationDate(self)
        action_par_date_de_modification.triggered.connect(self._sort_by_date)
        action_par_date_de_modification.setText(
            QCoreApplication.translate("UiExpleurer", "Par date de modification", None))

        action_simple = SimpleDisplay(self)
        action_simple.triggered.connect(self._display_simple)
        action_simple.setText(QCoreApplication.translate("UiExpleurer", "Simple", None))

        action_en_d_tails = DetailsDisplay(self)
        action_en_d_tails.triggered.connect(self._display_details)
        action_en_d_tails.setText(QCoreApplication.translate("UiExpleurer", "En d\u00e9tails", None))

        action_deplacement = MoveSomething(self)
        action_deplacement.triggered.connect(self._paste_something)
        action_deplacement.setText(QCoreApplication.translate("UiExpleurer", "D\u00e9poser", None))
        action_deplacement.setShortcut('CTRL+V')
        action_copier = CopySomething(self)
        action_copier.triggered.connect(self._copy_something)
        action_copier.setText(QCoreApplication.translate("UiExpleurer", "Copier", None))
        action_copier.setShortcut('CTRL+C')
        action_couper = CutSomething(self)
        action_couper.triggered.connect(self._cut_something)
        action_couper.setText(QCoreApplication.translate("UiExpleurer", "Couper", None))
        action_couper.setShortcut('CTRL+X')
        action_supprimer = DeleteSomething(self)
        action_supprimer.triggered.connect(self._delete_something)
        action_supprimer.setText(QCoreApplication.translate("UiExpleurer", "Supprimer", None))
        action_supprimer.setShortcut('Del')
        action_ouvrir = OpenSomething(self)
        action_ouvrir.triggered.connect(self._open_something)
        action_ouvrir.setText(QCoreApplication.translate("UiExpleurer", "Ouvrir", None))
        action_ouvrir.setShortcut('CTRL+O')

        action_renommer = RenameSomething(self)
        action_renommer.triggered.connect(self._rename_something)
        action_renommer.setText(QCoreApplication.translate("UiExpleurer", "Renommer", None))
        action_renommer.setShortcut('F2')

        return [
            action_cr_er_un_fichier,
            action_cr_er_un_dossier,
            action_cr_er_un_raccourci,
            action_par_nom,
            action_par_type,
            action_par_extension,
            action_par_date_de_modification,
            action_simple,
            action_en_d_tails,
            action_copier,
            action_couper,
            action_deplacement,
            action_supprimer,
            action_ouvrir,
            action_renommer
        ]

    def _rename_something(self):
        if self._tmp_search:
            return
        selected_item = self.table_widget.selectedItems()[0]
        name = self.table_widget.item(selected_item.row(), 0).text()
        dialog = RenameDialog(name, self._current_directory)
        dialog.exec()
        self._load_data_sorted(list(map(str, self._current_directory.iterdir())))

    def _open_something(self):
        selected_item = self.table_widget.selectedItems()[0]
        name = self.table_widget.item(selected_item.row(), 0).text()
        os.startfile(str((self._current_directory / name)))

    def _delete_something(self):
        if self._tmp_search:
            return
        selected_item = self.table_widget.selectedItems()[0]
        name = self.table_widget.item(selected_item.row(), 0).text()
        if (self._current_directory / name).is_dir():
            delete_directory(name, self._current_directory)
        elif (self._current_directory / name).suffix == '.lnk':
            delete_shortcut(name, self._current_directory)
        else:
            delete_file(name, self._current_directory)
        self._load_data_sorted(list(map(str, self._current_directory.iterdir())))

    def _copy_something(self):
        self._is_copy_active = True
        selected_item = self.table_widget.selectedItems()[0]
        self._tmp_file_move = self._current_directory / self.table_widget.item(selected_item.row(), 0).text()

    def _cut_something(self):
        self._is_copy_active = False
        selected_item = self.table_widget.selectedItems()[0]
        self._tmp_file_move = self._current_directory / self.table_widget.item(selected_item.row(), 0).text()

    def _paste_something(self):
        if not self._tmp_file_move:
            return
        if self._is_copy_active:
            if self._tmp_file_move.is_dir():
                copy_directory(str(self._tmp_file_move), self._tmp_file_move.name,
                               self._current_directory)
            elif self._tmp_file_move.suffix == '.lnk':
                copy_shortcut(str(self._tmp_file_move), self._tmp_file_move.name, self._current_directory)
            else:
                copy_file(str(self._tmp_file_move), self._tmp_file_move.name, self._current_directory)
            self._load_data_sorted(list(map(str, self._current_directory.iterdir())))
            return
        if self._tmp_file_move.is_dir():
            move_directory(self._tmp_file_move.name,
                           str(self._current_directory / self._tmp_file_move.name),
                           self._tmp_file_move.parent)
        elif self._tmp_file_move.suffix == '.lnk':
            move_shortcut(self._tmp_file_move.name,
                          str(self._current_directory / self._tmp_file_move.name),
                          self._tmp_file_move.parent)
        else:
            move_file(self._tmp_file_move.name,
                      str(self._current_directory / self._tmp_file_move.name), self._tmp_file_move.parent)
        self._load_data_sorted(list(map(str, self._current_directory.iterdir())))
        self._tmp_file_move = None

    def _main_windows_configuration(self):
        self.setObjectName("UiExpleurer")
        self.setWindowTitle(QCoreApplication.translate("UiExpleurer", "Expleurer", None))
        self.resize(800, 600)
        self.setFont(DefaultFont())

        if '--custom-style' in sys.argv:
            with open(Path(__file__).parent / 'qss' / 'style.qss', 'r') as qss_file:
                style = qss_file.read()
                self.setStyleSheet(style)

    def _layout_configuration(self):
        central_widget = QWidget(self)
        central_widget.setObjectName("centralwidget")

        self.tree_widget = QTreeWidget(central_widget)
        self.tree_widget.setObjectName("treeWidget")

        self.table_widget = QTableWidget(central_widget)
        self.table_widget.setObjectName("listView")
        self.table_widget.verticalHeader().hide()
        self.table_widget.doubleClicked.connect(self._goto_directory_by_table)
        self.table_widget.keyPressEvent = self._press_enter_on_directory
        self.table_widget.setContextMenuPolicy(Qt.ActionsContextMenu)
        self.table_widget.setAlternatingRowColors(True)

        # self.table_widget.itemClicked.connect(self._display_args)

        self.line_edit = QLineEdit(central_widget)
        self.line_edit.setText(str(self._current_directory))
        self.line_edit.editingFinished.connect(self._update_path)
        # self.table_widget.setHorizontalHeaderItem(0, QTableWidgetItem('Nom'))
        # self.table_widget.setHorizontalHeaderItem(1, QTableWidgetItem('Type'))
        # self.table_widget.setHorizontalHeaderItem(2, QTableWidgetItem('Date de modification'))

        # name_header = self.table_widget.horizontalHeaderItem(0)
        # name_header.setText(QCoreApplication.translate("UiExpleurer", "Nom", None))

        # self.table_widget.model().setHeaderData(1, Qt.Horizontal, 'Nom')

        vertical_layout = QVBoxLayout(central_widget)
        vertical_layout.setObjectName("verticalLayout")

        horizontal_layout = QHBoxLayout(vertical_layout.widget())
        horizontal_layout.setObjectName("horizontalLayout")
        horizontal_layout.addWidget(self.tree_widget)
        horizontal_layout.addWidget(self.table_widget)
        horizontal_layout.setStretch(0, 0)
        horizontal_layout.setStretch(1, 1)

        horizontal_layout1 = QHBoxLayout(vertical_layout.widget())
        horizontal_layout1.setObjectName("horizontalLayout")
        horizontal_layout1.addWidget(self.line_edit)
        horizontal_layout1.setStretch(0, 1)

        vertical_layout.addLayout(horizontal_layout1)
        vertical_layout.addLayout(horizontal_layout)

        central_widget.setLayout(vertical_layout)
        self.setCentralWidget(central_widget)

    def _press_enter_on_directory(self, event: QKeyEvent):
        if event.key() in [Qt.Key_Enter, Qt.Key_Enter - 1]:
            self.line_edit.setText(str(self.table_widget.selectedItems()[0].data(254)))
            self._update_path()

    def _update_path(self):
        text = self.line_edit.text()
        self._current_directory = (self._current_directory / text
                                   if (self._current_directory / text).is_dir() else
                                   self._current_directory)
        self._load_data_sorted(list(map(str, self._current_directory.iterdir())))

    def _goto_directory_by_table(self, model_index: QModelIndex):
        self._tmp_search = None
        item = self.table_widget.itemFromIndex(model_index)
        text = item.data(254)
        self._current_directory = (self._current_directory / text
                                   if (self._current_directory / text).is_dir() else
                                   self._current_directory)
        self.line_edit.setText(str(self._current_directory))
        self._load_data_sorted(list(map(str, self._current_directory.iterdir())))

    def _menu_configuration(self, actions: list[QAction]):
        menu_bar = MainMenuBar(self)
        menu_fichier = FileMenu(menu_bar)
        menu_fichier.setTitle(QCoreApplication.translate("UiExpleurer", "Fichier", None))
        menu_trier = SortMenu(menu_bar)
        menu_trier.setTitle(QCoreApplication.translate("UiExpleurer", "Trier", None))
        menu_afficher = DisplayMenu(menu_bar)
        menu_afficher.setTitle(QCoreApplication.translate("UiExpleurer", "Afficher", None))
        menu_rechercher = SearchSomething(menu_bar)
        menu_rechercher.setText(QCoreApplication.translate("UiExpleurer", "Rechercher", None))
        menu_rechercher.triggered.connect(self._search_files)
        menu_rechercher.setShortcut("CTRL+F")
        menu_aide = HelpMenu(menu_bar)
        menu_aide.setTitle(QCoreApplication.translate("UiExpleurer", "Aide", None))

        menu_bar.addAction(menu_fichier.menuAction())
        menu_bar.addAction(menu_trier.menuAction())
        menu_bar.addAction(menu_afficher.menuAction())
        menu_bar.addAction(menu_rechercher)
        menu_bar.addAction(menu_aide.menuAction())
        menu_fichier.addAction(actions[0])
        menu_fichier.addAction(actions[1])
        menu_fichier.addAction(actions[2])
        menu_trier.addAction(actions[3])
        menu_trier.addAction(actions[4])
        menu_trier.addAction(actions[5])
        menu_trier.addAction(actions[6])
        menu_afficher.addAction(actions[7])
        menu_afficher.addAction(actions[8])

        self.setMenuBar(menu_bar)
        self.table_widget.addAction(actions[13])
        self.table_widget.addAction(actions[14])
        self.table_widget.addAction(actions[9])
        self.table_widget.addAction(actions[10])
        self.table_widget.addAction(actions[11])
        self.table_widget.addAction(actions[12])

    def _create_file(self):
        dialog = FileDialog(self._current_directory)
        dialog.exec()
        self._load_data_sorted(list(map(str, self._current_directory.iterdir())))

    def _create_directory(self):
        dialog = DirectoryDialog(self._current_directory)
        dialog.exec()
        self._load_data_sorted(list(map(str, self._current_directory.iterdir())))

    def _create_shortcut(self):
        dialog = ShortcutDialog(self._current_directory)
        dialog.exec()
        self._load_data_sorted(list(map(str, self._current_directory.iterdir())))

    def _handle_result(self, result):
        self._tmp_search = result
        if self._tmp_search:
            self.line_edit.setText('Recherche de "' + self._search_term + '" dans ' + self._current_directory.name)
            self._load_data_sorted(self._tmp_search)
        else:
            self.line_edit.setText(str(self._current_directory))
            self._load_data_sorted(list(map(str, self._current_directory.iterdir())))

    def _handle_search_term(self, search_term):
        self._search_term = search_term
        self._worker_thread = WorkerSearchThread(self._search_term, self._current_directory)
        self._worker_thread.resultready.connect(self._handle_result)
        self._worker_thread.start()

    def _search_files(self):
        dialog = SearchDialog(self._current_directory)
        self._search_term = None

        dialog.pattern.connect(self._handle_search_term)
        dialog.exec()

    def _sort_by_type(self):
        self._sorted_by = sort_by_type
        if self._tmp_search:
            self._load_data_sorted(self._tmp_search)
        else:
            self._load_data_sorted(list(map(str, self._current_directory.iterdir())))

    def _sort_by_name(self):
        self._sorted_by = sort_by_name
        if self._tmp_search:
            self._load_data_sorted(self._tmp_search)
        else:
            self._load_data_sorted(list(map(str, self._current_directory.iterdir())))

    def _sort_by_extension(self):
        self._sorted_by = sort_by_extension
        if self._tmp_search:
            self._load_data_sorted(self._tmp_search)
        else:
            self._load_data_sorted(list(map(str, self._current_directory.iterdir())))

    def _sort_by_date(self):
        self._sorted_by = sort_by_date
        if self._tmp_search:
            self._load_data_sorted(self._tmp_search)
        else:
            self._load_data_sorted(list(map(str, self._current_directory.iterdir())))

    def _display_simple(self):
        self._is_display_simple = True
        if self._tmp_search:
            self._load_data_sorted(self._tmp_search)
        else:
            self._load_data_sorted(list(map(str, self._current_directory.iterdir())))

    def _display_details(self):
        self._is_display_simple = False
        if self._tmp_search:
            self._load_data_sorted(self._tmp_search)
        else:
            self._load_data_sorted(list(map(str, self._current_directory.iterdir())))

    def _load_data_sorted(self, data: list[str]) -> None:
        files_in_working_directory = list(map(Path, self._sorted_by(data)))
        self.table_widget.clearContents()
        modified = datetime.fromtimestamp(self._current_directory.stat().st_mtime, tz=timezone.utc)
        all_files_in_directory = [right_file for right_file in files_in_working_directory if
                                  not right_file.stat().st_file_attributes & FILE_ATTRIBUTE_SYSTEM and
                                  not right_file.stat().st_file_attributes & FILE_ATTRIBUTE_NOT_CONTENT_INDEXED and
                                  not right_file.stat().st_file_attributes & FILE_ATTRIBUTE_READONLY]
        self.table_widget.setRowCount(1 + len(all_files_in_directory))
        file_widget = QTableWidgetItem(QIcon(str(Path(__file__) / '..' / './img/folder.svg')), '..')
        file_widget.setData(254, self._current_directory.parent)
        self.table_widget.setItem(0, 0, file_widget)
        if self._is_display_simple:
            self.table_widget.setColumnCount(1)
            self.table_widget.setItem(0, 1, QTableWidgetItem(''))
            self.table_widget.setItem(0, 2, QTableWidgetItem(''))
        else:
            self.table_widget.setColumnCount(3)
            self.table_widget.setHorizontalHeaderLabels(['Nom', 'Type', 'Date de modification'])
            self.table_widget.setItem(0, 1, QTableWidgetItem(suffix_to_type(str(self._current_directory))))
            self.table_widget.setItem(0, 2, QTableWidgetItem(str(modified)))

        for index, file in enumerate(all_files_in_directory):
            modified = datetime.fromtimestamp(file.stat().st_mtime, tz=timezone.utc)
            if file.is_file():
                icon = str(Path(__file__) / '..' / './img/file.svg')
            elif file.is_dir():
                icon = str(Path(__file__) / '..' / './img/folder.svg')
            else:
                continue
            file_widget = QTableWidgetItem(QIcon(icon), str(file.name))
            file_widget.setData(254, file)
            self.table_widget.setItem(index + 1, 0, file_widget)
            if self._is_display_simple:
                self.table_widget.setItem(index + 1, 1, QTableWidgetItem(''))
                self.table_widget.setItem(index + 1, 2, QTableWidgetItem(''))
            else:
                self.table_widget.setItem(index + 1, 1, QTableWidgetItem(suffix_to_type(str(file))))
                self.table_widget.setItem(index + 1, 2, QTableWidgetItem(str(modified)))
        self.table_widget.horizontalHeader().setSectionResizeMode(QHeaderView.Stretch)

    def _load_data(self):
        self.tree_widget.setHeaderHidden(True)

        items = []
        devices = [x.device.removesuffix(':\\') for x in disk_partitions() if x.fstype == 'NTFS']
        tree_data = {'Utilisateur': [
            'Bureau',
            'Documents',
            'Images',
            'Vidéos',
            'Musique',
            'Téléchargement',
            'AppData'
        ],
            'Périphérique': [f"Disque {device}" for device in devices]
        }
        for key, values in tree_data.items():
            item = QTreeWidgetItem([key])
            for value in values:
                child = QTreeWidgetItem([value])
                item.addChild(child)

            items.append(item)
        self.tree_widget.insertTopLevelItems(0, items)
        self.tree_widget.expandToDepth(0)
        self.tree_widget.itemPressed.connect(self._goto_directory)

    def _goto_directory(self, item: QTreeWidgetItem):
        internal_map = {'Bureau': Path().home() / 'Desktop',
                        'Documents': Path().home() / 'Documents',
                        'Images': Path().home() / 'Pictures',
                        'Vidéos': Path().home() / 'Videos',
                        'Musique': Path().home() / 'Music',
                        'AppData': Path().home() / 'AppData' / 'Roaming',
                        'Téléchargement': Path().home() / 'Downloads',
                        'Disque C': Path('C:\\')}
        self._current_directory = internal_map.get(item.text(0), Path().home())
        self.line_edit.setText(str(self._current_directory))
        self._load_data_sorted(list(map(str, self._current_directory.iterdir())))


if __name__ == '__main__':
    app = QApplication(sys.argv)
    ui = UiExpleurer()
    app.exec()
