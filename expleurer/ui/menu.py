"""All the menus present in the GUI"""
from PySide6.QtCore import QObject, QRect
from PySide6.QtWidgets import QMenuBar, QMenu, QWidget


class MainMenuBar(QMenuBar):
    """This is the default menu bar"""

    def __init__(self, parent: QObject | None):
        super().__init__(parent)
        self.setObjectName("menubar")
        self.setGeometry(QRect(0, 0, 799, 24))


class FileMenu(QMenu):
    """This is the file menu"""

    def __init__(self, parent: QWidget):
        super().__init__(parent)
        self.setObjectName("menuFile")


class SortMenu(QMenu):
    """This is the sort menu"""

    def __init__(self, parent: QWidget):
        super().__init__(parent)
        self.setObjectName("menuSort")


class DisplayMenu(QMenu):
    """This is the display menu"""

    def __init__(self, parent: QWidget):
        super().__init__(parent)
        self.setObjectName("menuDisplay")


class HelpMenu(QMenu):
    """This is the help menu"""

    def __init__(self, parent: QWidget):
        super().__init__(parent)
        self.setObjectName("menuAide")
