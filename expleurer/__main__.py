import sys

from PySide6.QtWidgets import QApplication

from expleurer.ui.mainwindows import UiExpleurer

if __name__ == '__main__':
    app = QApplication(sys.argv)
    ui = UiExpleurer()
    app.exec()
