"""Module according to file view manipulation"""
from pathlib import Path
from stat import FILE_ATTRIBUTE_SYSTEM, FILE_ATTRIBUTE_NOT_CONTENT_INDEXED, FILE_ATTRIBUTE_READONLY
from typing import List

from expleurer.structure.typefile import suffix_to_type


def sort_by_type(files_in_directory: List[str]) -> List[str]:
    """
    Sorted files in directory by type

    :param files_in_directory: all the files in directory
    :type files_in_directory: List[str]
    :return: sorted files by modified name
    :rtype: List[str]
    """
    return sorted(files_in_directory, key=lambda key: (suffix_to_type(key), Path(key).stem))


def sort_by_extension(files_in_directory: List[str]) -> List[str]:
    """
    Sorted files in directory by extension

    :param files_in_directory: all the files in directory
    :type files_in_directory: List[str]
    :return: sorted files by modified name
    :rtype: List[str]
    """
    return sorted(files_in_directory, key=lambda key: (Path(key).suffix, Path(key).stem))


def sort_by_name(files_in_directory: List[str]) -> List[str]:
    """
    Sorted files in directory by name

    :param files_in_directory: all the files in directory
    :type files_in_directory: List[str]
    :return: sorted files by modified name
    :rtype: List[str]
    """
    return sorted(files_in_directory)


def sort_by_date(files_in_directory: List[str]) -> List[str]:
    """
    Sorted files in directory by modified date

    :param files_in_directory: all the files in directory
    :type files_in_directory: List[str]
    :return: sorted files by modified date
    :rtype: List[str]
    """
    return sorted(files_in_directory, key=lambda key: Path(key).stat().st_mtime)


def search_in_path(thing_to_search: str, path_to_search: str) -> List[str]:
    """
    Search something (directory, file, shortcut) in the path

    :param thing_to_search: the pattern to find
    :type thing_to_search: str
    :param path_to_search: the path where to search
    :return: List of all file, directory and shortcut name matching the thing to search
    :rtype: List[str]
    """
    return [str(file) for file in Path(path_to_search).rglob('*' + thing_to_search + '*') if
            not file.stat().st_file_attributes & FILE_ATTRIBUTE_SYSTEM and
            not file.stat().st_file_attributes & FILE_ATTRIBUTE_NOT_CONTENT_INDEXED and
            not file.stat().st_file_attributes & FILE_ATTRIBUTE_READONLY]
