"""Module according to file type manipulation"""
from pathlib import Path


def suffix_to_type(file_name: str) -> str:
    """
    Return the type of the file name (Other, Video, Image, Text...)

    :param file_name: The filename (stem in Path module)
    :type: str
    :return: The type according to the filename
    :rtype: str
    """
    if Path(file_name).is_dir():
        return 'Repertoire'
    suffix_to_type_dict = {
        '.sh': 'Texte',
        '.py': 'Texte',
        '.txt': 'Texte',
        '.js': 'Texte',
        '.csv': 'Texte',
        '.json': 'Texte',
        '.html': 'Texte',
        '.numbers': 'Texte',
        '.doc': 'Texte',
        '.xlsx': 'Texte',
        '.odt': 'Texte',
        '.ppt': 'Texte',
        '.pdf': 'Texte',
        '.pptx': 'Texte',
        '.docx': 'Texte',
        '.key': 'Texte',
        '.odp': 'Texte',
        '.pages': 'Texte',
        '.xls': 'Texte',
        '.ods': 'Texte',
        '.css': 'Texte',
        '.mp3': 'Audio',
        '.flac': 'Audio',
        '.wav': 'Audio',
        '.avi': 'Video',
        '.webm': 'Video',
        '.mov': 'Video',
        '.mp4': 'Video',
        '.bmp': 'Image',
        '.tiff': 'Image',
        '.jpg': 'Image',
        '.png': 'Image',
        '.jpeg': 'Image',
        '.gif': 'Image',
    }
    point_index = file_name.rfind('.')
    return suffix_to_type_dict.get(file_name[point_index:] if 0 < point_index < len(file_name) - 1 else '', 'Autre')
