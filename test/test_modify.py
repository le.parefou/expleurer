"""Test module according to src/modify"""
import sys
import unittest
from unittest import TestCase

from expleurer.modify import *


class TestModifyConcept(TestCase):
    """Test class according to ModifyFeature"""

    def test_create_file(self) -> None:
        """
        Test create file class method

        :return: Nothing
        """
        current_working_dir = Path().cwd()

        # Use case 1 : test create file
        create_file('tests', current_working_dir)

        self.assertTrue((current_working_dir / 'tests').exists())
        self.assertTrue((current_working_dir / 'tests').is_file())

        # Use case 2 : file already exist, cannot create
        with self.assertRaises(FileExistsError):
            create_file('tests', current_working_dir)

        if (current_working_dir / 'tests').exists():
            (current_working_dir / 'tests').unlink()

        # Use case 3 : file is empty
        with self.assertRaises(ValueError):
            create_file('', current_working_dir)

    def test_create_directory(self) -> None:
        """
        Test create directory class method

        :return: Nothing
        """
        current_working_dir = Path().cwd()

        # Use case 1 : create a directory
        create_directory('tests', current_working_dir)
        self.assertTrue((current_working_dir / 'tests').exists())
        self.assertTrue((current_working_dir / 'tests').is_dir())

        # Use case 2 : directory already exists
        with self.assertRaises(FileExistsError):
            create_directory('tests', current_working_dir)

        if (current_working_dir / 'tests').exists():
            (current_working_dir / 'tests').rmdir()

        # Use case 3 ; directory name is empty
        with self.assertRaises(ValueError):
            create_directory('', current_working_dir)

    def test_create_shortcut(self) -> None:
        """
        Test create shortcut class method

        :return: Nothing
        """
        current_working_dir = Path().cwd()

        # Use case 1 : create a shortcut
        create_shortcut('tests.lnk', sys.executable, current_working_dir)

        # Use case 2 : shortcut already exists
        create_shortcut('tests.lnk', sys.executable, current_working_dir)

        # Use case 3 : shortcut is malformed
        with self.assertRaises(ValueError):
            create_shortcut('tests', sys.executable, current_working_dir)

        self.assertFalse((current_working_dir / 'tests').exists())
        self.assertTrue((current_working_dir / 'tests.lnk').exists())

        if (current_working_dir / 'tests.lnk').exists():
            (current_working_dir / 'tests.lnk').unlink()

        # Use case 4 : shortcut name is empty
        with self.assertRaises(ValueError):
            create_shortcut('', sys.executable, current_working_dir)

        # Use case 5 : shortcut target is empty
        with self.assertRaises(ValueError):
            create_shortcut('test', '', current_working_dir)

        # Use case 6 : shortcut target is not exists
        with self.assertRaises(FileNotFoundError):
            create_shortcut('test.lnk', 'lol', current_working_dir)

    def test_rename_file(self) -> None:
        """
        Test rename file class method

        :return: Nothing
        """
        current_working_dir = Path().cwd()

        # Use case 1 : rename a file
        create_file('tests', current_working_dir)
        rename_file('tests', 'tests2', current_working_dir)

        self.assertTrue((current_working_dir / 'tests2').exists())
        self.assertTrue((current_working_dir / 'tests2').is_file())
        self.assertFalse((current_working_dir / 'tests').exists())

        # Use case 2 : file already exists, cannot rename
        create_file('tests3', current_working_dir)
        with self.assertRaises(FileExistsError):
            rename_file('tests2', 'tests3', current_working_dir)

        if (current_working_dir / 'tests2').exists():
            (current_working_dir / 'tests2').unlink()
        if (current_working_dir / 'tests3').exists():
            (current_working_dir / 'tests3').unlink()

    def test_rename_directory(self) -> None:
        """
        Test rename directory class method

        :return: Nothing
        """
        current_working_dir = Path().cwd()

        # Use case 1 : rename a directory
        create_directory('tests', current_working_dir)
        rename_directory('tests', 'tests2', current_working_dir)
        self.assertTrue((current_working_dir / 'tests2').exists())
        self.assertTrue((current_working_dir / 'tests2').is_dir())
        self.assertFalse((current_working_dir / 'tests').exists())

        # Use case 2 : directory already exists, cannot rename
        create_directory('tests3', current_working_dir)
        with self.assertRaises(FileExistsError):
            rename_directory('tests2', 'tests3', current_working_dir)

        if (current_working_dir / 'tests2').exists():
            (current_working_dir / 'tests2').rmdir()
        if (current_working_dir / 'tests3').exists():
            (current_working_dir / 'tests3').rmdir()

    def test_rename_shortcut(self) -> None:
        """
        Test rename shortcut class method

        :return: Nothing
        """
        current_working_dir = Path().cwd()

        # Use case 1 : shortcut is malformed, cannot rename
        create_shortcut('tests.lnk', sys.executable, current_working_dir)
        with self.assertRaises(ValueError):
            rename_shortcut('tests.lnk', 'tests2', current_working_dir)

        # Use case 2 : shortcut rename
        rename_shortcut('tests.lnk', 'tests2.lnk', current_working_dir)
        self.assertTrue((current_working_dir / 'tests2.lnk').exists())
        self.assertFalse((current_working_dir / 'tests.lnk').exists())

        # Use case 3 : shortcut already exists, cannot rename
        create_shortcut('tests3.lnk', sys.executable, current_working_dir)
        with self.assertRaises(FileExistsError):
            rename_shortcut('tests2.lnk', 'tests3.lnk', current_working_dir)

        if (current_working_dir / 'tests2.lnk').exists():
            (current_working_dir / 'tests2.lnk').unlink()
        if (current_working_dir / 'tests3.lnk').exists():
            (current_working_dir / 'tests3.lnk').unlink()

    def test_delete_file(self) -> None:
        """
        Test delete file class method

        :return: Nothing
        """
        current_working_dir = Path().cwd()

        # Use case 1 : delete file
        create_file('tests', current_working_dir)

        self.assertTrue((current_working_dir / 'tests').exists())
        self.assertTrue((current_working_dir / 'tests').is_file())

        delete_file('tests', current_working_dir)

        self.assertFalse((current_working_dir / 'tests').exists())

        # Use case 2 : file doesn't exist, cannot delete
        with self.assertRaises(FileNotFoundError):
            delete_file('tests', current_working_dir)

        # Use case 3 : the path is a file
        create_file('tests', current_working_dir)
        with self.assertRaises(NotADirectoryError):
            delete_file('tests', current_working_dir / 'tests')
        delete_file('tests', current_working_dir)

    def test_delete_directory(self) -> None:
        """
        Test delete directory class method

        :return: Nothing
        """
        current_working_dir = Path().cwd()

        # Use case 1 : delete directory (empty)
        create_directory('tests', current_working_dir)

        self.assertTrue((current_working_dir / 'tests').exists())
        self.assertTrue((current_working_dir / 'tests').is_dir())

        delete_directory('tests', current_working_dir)

        self.assertFalse((current_working_dir / 'tests').exists())

        # Use case 2 : directory doesn't exist, cannot delete
        with self.assertRaises(FileNotFoundError):
            delete_directory('tests', current_working_dir)

        # Use case 3 : delete directory (full)
        create_directory('tests', current_working_dir)
        create_directory('tests/test2', current_working_dir)
        delete_directory('tests', current_working_dir)
        self.assertFalse((current_working_dir / 'tests').exists())

        # Use case 4 : a file on the directory name
        create_file('tests1', current_working_dir)
        with self.assertRaises(NotADirectoryError):
            delete_directory('tests1', current_working_dir)

        # Use case 5 : a file on the path
        with self.assertRaises(NotADirectoryError):
            delete_directory('test', current_working_dir / 'tests1')
        delete_file('tests1', current_working_dir)

    def test_delete_shortcut(self) -> None:
        """
        Test delete shortcut class method

        :return: Nothing
        """
        current_working_dir = Path().cwd()

        # Use case 1 : delete shortcut
        create_shortcut('tests.lnk', sys.executable, current_working_dir)
        self.assertTrue((current_working_dir / 'tests.lnk').exists())

        delete_shortcut('tests.lnk', current_working_dir)
        self.assertFalse((current_working_dir / 'tests.lnk').exists())

        # Use case 2 : shortcut doesn't exist, cannot delete
        with self.assertRaises(FileNotFoundError):
            delete_shortcut('tests.lnk', current_working_dir)

        # Use case 3 : shortcut with wrong suffix
        with self.assertRaises(ValueError):
            delete_shortcut('tests.lte', current_working_dir)

    def test_move_file(self) -> None:
        """
        Test move file class method

        :return: Nothing
        """
        current_working_dir = Path().cwd()

        # Use case 1 : Move file
        create_file('tests', current_working_dir)
        create_directory('dossier', current_working_dir)
        move_file('tests', 'dossier/tests', current_working_dir)

        self.assertFalse((current_working_dir / 'tests').exists())
        self.assertTrue((current_working_dir / 'dossier' / 'tests').exists())
        self.assertTrue((current_working_dir / 'dossier' / 'tests').is_file())

        # Use case 2 : file already exists, cannot move
        create_file('tests', current_working_dir)
        with self.assertRaises(FileExistsError):
            move_file('tests', 'dossier/tests', current_working_dir)

        if (current_working_dir / 'dossier' / 'tests').exists():
            (current_working_dir / 'dossier' / 'tests').unlink()

        if (current_working_dir / 'tests').exists():
            (current_working_dir / 'tests').unlink()

        if (current_working_dir / 'dossier').exists():
            (current_working_dir / 'dossier').rmdir()

    def test_move_directory(self) -> None:
        """
        Test move directory class method

        :return: Nothing
        """
        current_working_dir = Path().cwd()

        # Use case 1 : Move directory
        create_directory('tests', current_working_dir)
        create_directory('dossier', current_working_dir)
        move_directory('tests', 'dossier/tests', current_working_dir)

        self.assertFalse((current_working_dir / 'tests').exists())
        self.assertTrue((current_working_dir / 'dossier' / 'tests').exists())
        self.assertTrue((current_working_dir / 'dossier' / 'tests').is_dir())

        # Use case 2 : directory already exists, cannot move
        create_directory('tests', current_working_dir)

        with self.assertRaises(FileExistsError):
            move_directory('tests', 'dossier/tests', current_working_dir)

        # Use case 3 : initial name is a file
        create_file('test2', current_working_dir)
        with self.assertRaises(NotADirectoryError):
            move_directory('test2', 'tests/test2', current_working_dir)

        if (current_working_dir / 'dossier' / 'tests').exists():
            (current_working_dir / 'dossier' / 'tests').rmdir()

        if (current_working_dir / 'dossier').exists():
            (current_working_dir / 'dossier').rmdir()
        if (current_working_dir / 'tests').exists():
            (current_working_dir / 'tests').rmdir()
        if (current_working_dir / 'test2').exists():
            (current_working_dir / 'test2').unlink()

    def test_move_shortcut(self):
        """
        Test move shortcut class method

        :return: Nothing
        """
        current_working_dir = Path().cwd()

        # Use case 1 : move shortcut
        create_shortcut('tests.lnk', sys.executable, current_working_dir)
        create_directory('dossier', current_working_dir)
        move_shortcut('tests.lnk', 'dossier/tests.lnk', current_working_dir)

        self.assertFalse((current_working_dir / 'tests.lnk').exists())
        self.assertTrue((current_working_dir / 'dossier' / 'tests.lnk').exists())

        # Use case 2 : shortcut malformed, cannot move
        with self.assertRaises(ValueError):
            move_shortcut('dossier/tests.lnk', 'dossier/tests.lk', current_working_dir)

        # Use case 3 : shortcut already exists, cannot move
        create_shortcut('tests.lnk', sys.executable, current_working_dir)
        with self.assertRaises(FileExistsError):
            move_file('tests.lnk', 'dossier/tests.lnk', current_working_dir)

        if (current_working_dir / 'dossier' / 'tests.lnk').exists():
            (current_working_dir / 'dossier' / 'tests.lnk').unlink()
        if (current_working_dir / 'tests.lnk').exists():
            (current_working_dir / 'tests.lnk').unlink()
        if (current_working_dir / 'dossier').exists():
            (current_working_dir / 'dossier').rmdir()

    def test_copy_file(self):
        """
        Test copy file class method

        :return: Nothing
        """
        current_working_dir = Path().cwd()
        content = "Hello awesome world"

        # Use case 1 : copy file
        create_file('tests', current_working_dir)
        (current_working_dir / 'tests').write_text(content)
        copy_file('tests', 'test_copy', current_working_dir)

        self.assertTrue((current_working_dir / 'tests').exists())
        self.assertTrue((current_working_dir / 'tests').is_file())

        self.assertTrue((current_working_dir / 'test_copy').exists())
        self.assertTrue((current_working_dir / 'test_copy').is_file())

        content_return = (current_working_dir / 'test_copy').read_text()
        self.assertEqual(content, content_return)

        if (current_working_dir / 'tests').exists():
            (current_working_dir / 'tests').unlink()
        if (current_working_dir / 'test_copy').exists():
            (current_working_dir / 'test_copy').unlink()

    def test_copy_directory(self):
        """
        Test copy directory class method

        :return: Nothing
        """
        current_working_dir = Path().cwd()

        # Use case 1 : copy directory
        create_directory('tests', current_working_dir)
        copy_directory('tests', 'test_copy', current_working_dir)

        self.assertTrue((current_working_dir / 'tests').exists())
        self.assertTrue((current_working_dir / 'tests').is_dir())

        self.assertTrue((current_working_dir / 'test_copy').exists())
        self.assertTrue((current_working_dir / 'test_copy').is_dir())

        create_directory('tests/tests2', current_working_dir)
        create_file('tests/tests3', current_working_dir)
        create_shortcut('tests/tests4.lnk', sys.executable, current_working_dir)
        copy_directory('tests', 'test_copy2', current_working_dir)

        self.assertTrue((current_working_dir / 'test_copy2').exists())
        self.assertTrue((current_working_dir / 'test_copy2').is_dir())

        self.assertTrue((current_working_dir / 'test_copy2/tests3').exists())
        self.assertTrue((current_working_dir / 'test_copy2/tests3').is_file())

        self.assertTrue((current_working_dir / 'test_copy2/tests4.lnk').exists())
        with shortcut(str((current_working_dir / 'test_copy2/tests4.lnk').resolve())) as my_shortcut:
            source_return = my_shortcut.path
        with shortcut(str((current_working_dir / 'tests/tests4.lnk').resolve())) as my_shortcut:
            self.assertEqual(source_return, my_shortcut.path)

        self.assertTrue((current_working_dir / 'test_copy2/tests2').exists())
        self.assertTrue((current_working_dir / 'test_copy2/tests2').is_dir())

        if (current_working_dir / 'tests/tests2').exists():
            (current_working_dir / 'tests/tests2').rmdir()
        if (current_working_dir / 'tests/tests3').exists():
            (current_working_dir / 'tests/tests3').unlink()
        if (current_working_dir / 'tests/tests4.lnk').exists():
            (current_working_dir / 'tests/tests4.lnk').unlink()
        if (current_working_dir / 'test_copy2/tests2').exists():
            (current_working_dir / 'test_copy2/tests2').rmdir()
        if (current_working_dir / 'test_copy2/tests3').exists():
            (current_working_dir / 'test_copy2/tests3').unlink()
        if (current_working_dir / 'test_copy2/tests4.lnk').exists():
            (current_working_dir / 'test_copy2/tests4.lnk').unlink()

        if (current_working_dir / 'tests').exists():
            (current_working_dir / 'tests').rmdir()
        if (current_working_dir / 'test_copy').exists():
            (current_working_dir / 'test_copy').rmdir()
        if (current_working_dir / 'test_copy2').exists():
            (current_working_dir / 'test_copy2').rmdir()

    def test_copy_shortcut(self) -> None:
        """
        Test copy shortcut class method

        :return: Nothing
        """
        current_working_dir = Path().cwd()

        # Use case 1 : copy shortcut
        shortcut_source = sys.executable
        create_shortcut('tests.lnk', shortcut_source, current_working_dir)
        copy_shortcut('tests.lnk', 'test_copy.lnk', current_working_dir)
        self.assertTrue((current_working_dir / 'tests.lnk').exists())
        self.assertTrue((current_working_dir / 'test_copy.lnk').exists())
        with shortcut(str((current_working_dir / 'test_copy.lnk').resolve())) as my_shortcut:
            source_return = my_shortcut.path
        self.assertEqual(sys.executable, source_return)

        # Use case 2 : shortcut malformed, cannot copy
        with self.assertRaises(ValueError):
            copy_shortcut('tests.lnk', 'test_copy.txxt', current_working_dir)

        if (current_working_dir / 'tests.lnk').exists():
            (current_working_dir / 'tests.lnk').unlink()
        if (current_working_dir / 'test_copy.lnk').exists():
            (current_working_dir / 'test_copy.lnk').unlink()


if __name__ == '__main__':
    unittest.main()
