"""Test module according to src/ui/dialog"""
import sys
from pathlib import Path

from expleurer.ui.dialog import DirectoryDialog, FileDialog, ShortcutDialog, RenameDialog, ErrorDialog


def test_error_dialog_print(qtbot):
    """
    test error dialog
    """
    error = 'My awesome error'
    window = ErrorDialog(error)
    window.show()
    qtbot.addWidget(window)
    assert error == window.label_message.text()
    assert window.isActiveWindow()
    window._accepted()
    assert not window.isActiveWindow()


def test_create_file(qtbot, tmpdir):
    """
    test to create a file
    """
    window = FileDialog(Path(tmpdir))
    window.show()
    qtbot.addWidget(window)
    qtbot.keyClicks(window.edit_name, 'file.txt')
    window._accepted()

    assert (tmpdir / 'file.txt').exists()
    assert (tmpdir / 'file.txt').isfile()
    assert not window.isActiveWindow()

    window = FileDialog(Path(tmpdir))
    window.show()
    window.close()
    assert not window.isActiveWindow()

    window = FileDialog(Path(tmpdir))
    window.show()
    qtbot.addWidget(window)
    window._accepted()
    qtbot.keyClicks(window.edit_name, 'file.txt')
    window._accepted()
    qtbot.keyClicks(window.edit_name, 'file.txt')
    window._accepted()


def test_create_directory(qtbot, tmpdir):
    """
    test to create a directory
    """
    window = DirectoryDialog(Path(tmpdir))
    window.show()
    qtbot.addWidget(window)
    qtbot.keyClicks(window.edit_name, 'video')
    window._accepted()

    assert (tmpdir / 'video').exists()
    assert (tmpdir / 'video').isdir()
    assert not window.isActiveWindow()

    window = DirectoryDialog(Path(tmpdir))
    window.show()
    window.close()
    assert not window.isActiveWindow()

    window = DirectoryDialog(Path(tmpdir))
    window.show()
    qtbot.addWidget(window)
    window._accepted()
    assert "Le répertoire ne peut pas être vide." == window.dialog.label_message.text()
    qtbot.keyClicks(window.edit_name, 'video')
    window._accepted()
    assert "Le nom du répertoire existe déjà : renommez-le." == window.dialog.label_message.text()
    qtbot.keyClicks(window.edit_name, '?video')
    window._accepted()
    assert 'Un caractère spéciale n\'est pas autorisé : | / \ ? * < > "' == window.dialog.label_message.text()


def test_create_shortcut(qtbot, tmpdir):
    """
    test to create a shortcut
    """
    window = ShortcutDialog(Path(tmpdir))
    window.show()
    qtbot.addWidget(window)
    qtbot.keyClicks(window.edit_name, 'link.lnk')
    qtbot.keyClicks(window.edit_target, sys.executable)
    window._accepted()

    assert (tmpdir / 'link.lnk').exists()
    assert not window.isActiveWindow()

    window = ShortcutDialog(Path(tmpdir))
    window.show()
    window.close()
    assert not window.isActiveWindow()

    window = ShortcutDialog(Path(tmpdir))
    window.show()
    qtbot.addWidget(window)
    qtbot.keyClicks(window.edit_target, sys.executable)
    window._accepted()
    qtbot.keyClicks(window.edit_name, 'link1.lnk')
    qtbot.keyClicks(window.edit_target, '')
    window._accepted()
    qtbot.keyClicks(window.edit_name, '')
    qtbot.keyClicks(window.edit_name, sys.executable)
    qtbot.keyClicks(window.edit_target, 'link.lnk')
    window._accepted()
    qtbot.keyClicks(window.edit_name, 'link1.lnk')
    qtbot.keyClicks(window.edit_target, '')
    qtbot.keyClicks(window.edit_target, 'video')
    window._accepted()


def test_rename_file(qtbot, tmpdir):
    """
    test to rename a file
    """
    name = 'video1.avi'
    tmpdir.join(name).ensure()
    window = RenameDialog(name, Path(tmpdir))
    window.show()
    qtbot.addWidget(window)
    window.edit_name.clear()
    qtbot.keyClicks(window.edit_name, 'video2.str')
    window._accepted()

    assert (tmpdir / 'video2.str').exists()
    assert (tmpdir / 'video2.str').isfile()
    assert not window.isActiveWindow()

    name = 'video1.lnk'
    tmpdir.join(name).ensure()
    window = RenameDialog(name, Path(tmpdir))
    window.show()
    qtbot.addWidget(window)
    window.edit_name.clear()
    qtbot.keyClicks(window.edit_name, 'video2.lnk')
    window._accepted()

    assert (tmpdir / 'video2.lnk').exists()
    assert (tmpdir / 'video2.lnk').isfile()
    assert not window.isActiveWindow()

    tmpdir.join(name).ensure()
    window = RenameDialog(name, Path(tmpdir))
    window.show()
    qtbot.addWidget(window)
    window.edit_name.clear()
    qtbot.keyClicks(window.edit_name, 'video3')
    window._accepted()

    assert (tmpdir / 'video3.lnk').exists()
    assert (tmpdir / 'video3.lnk').isfile()
    assert not window.isActiveWindow()

    name = 'video'
    tmpdir.join(name).mkdir()
    window = RenameDialog(name, Path(tmpdir))
    window.show()
    qtbot.addWidget(window)
    window.edit_name.clear()
    qtbot.keyClicks(window.edit_name, 'video2')
    window._accepted()

    assert (tmpdir / 'video2').exists()
    assert (tmpdir / 'video2').isdir()
    assert not window.isActiveWindow()

    window = RenameDialog(name, Path(tmpdir))
    window.show()
    window.close()
    assert not window.isActiveWindow()
