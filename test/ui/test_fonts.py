"""Test module according to src/ui/font"""
from expleurer.ui.fonts import DefaultFont


def test_default_font():
    """Test the default font"""
    font = DefaultFont()
    assert font.family() == "Roboto"
    assert font.pointSize() == 12
