"""Test module according to src/ui/mainwindows"""
from pathlib import Path

from PySide6.QtCore import Qt
from PySide6.QtGui import QAction
from PySide6.QtWidgets import QMenu

from expleurer.ui.mainwindows import UiExpleurer


def test_top_actions(qtbot):
    """Test top actions on the main windows"""
    windows = UiExpleurer()
    windows.show()
    qtbot.addWidget(windows)
    windows.findChild(QMenu, 'menuFile').menuAction().trigger()
    windows.findChild(QAction, 'actionCreate_a_file').trigger()
    windows.findChild(QAction, 'actionCreate_a_directory').trigger()
    windows.findChild(QAction, 'actionCreate_a_shortcut').trigger()

    windows.findChild(QMenu, 'menuSort').menuAction().trigger()
    windows.findChild(QAction, 'actionBy_name').trigger()
    windows.findChild(QAction, 'actionBy_type').trigger()
    windows.findChild(QAction, 'actionBy_extension').trigger()
    windows.findChild(QAction, 'actionBy_modification_date').trigger()

    windows.findChild(QMenu, 'menuDisplay').menuAction().trigger()
    windows.findChild(QAction, 'actionSimple').trigger()
    windows.findChild(QAction, 'actionIn_detail').trigger()

    windows.findChild(QAction, 'actionSearch').trigger()


def test_create_rename_delete_a_file(qtbot):
    """Test create rename delete a file on the main windows"""
    windows = UiExpleurer()
    windows.show()
    qtbot.addWidget(windows)
    windows.findChild(QMenu, 'menuFile').menuAction().trigger()
    windows.findChild(QAction, 'actionCreate_a_file').trigger()
    item = windows.table_widget.findItems('test.txt', Qt.MatchContains)[0]
    windows.table_widget.item(item.row(), item.column()).setSelected(True)
    windows.findChild(QAction, 'actionRename').trigger()
    item = windows.table_widget.findItems('test2.txt', Qt.MatchContains)[0]
    windows.table_widget.item(item.row(), item.column()).setSelected(True)
    windows.findChild(QAction, 'actionDelete').trigger()


def test_create_rename_delete_a_directory(qtbot):
    """Test create rename delete a directory on the main windows"""
    windows = UiExpleurer()
    windows.show()
    qtbot.addWidget(windows)
    windows.findChild(QMenu, 'menuFile').menuAction().trigger()
    windows.findChild(QAction, 'actionCreate_a_directory').trigger()
    item = windows.table_widget.findItems('test', Qt.MatchContains)[0]
    windows.table_widget.item(item.row(), item.column()).setSelected(True)
    windows.findChild(QAction, 'actionRename').trigger()
    item = windows.table_widget.findItems('test2', Qt.MatchContains)[0]
    windows.table_widget.item(item.row(), item.column()).setSelected(True)
    windows.findChild(QAction, 'actionDelete').trigger()


def test_create_rename_delete_a_shortcut(qtbot):
    """Test create rename delete a shortcut on the main windows"""
    windows = UiExpleurer()
    windows.show()
    qtbot.addWidget(windows)
    windows.findChild(QMenu, 'menuFile').menuAction().trigger()
    windows.findChild(QAction, 'actionCreate_a_shortcut').trigger()
    item = windows.table_widget.findItems('test.lnk', Qt.MatchContains)[0]
    windows.table_widget.item(item.row(), item.column()).setSelected(True)
    windows.findChild(QAction, 'actionRename').trigger()
    item = windows.table_widget.findItems('test2.lnk', Qt.MatchContains)[0]
    windows.table_widget.item(item.row(), item.column()).setSelected(True)
    windows.findChild(QAction, 'actionDelete').trigger()


def test_open_file_with_default_app(qtbot):
    """Test open file with default app on the main windows"""
    windows = UiExpleurer()
    windows.show()
    qtbot.addWidget(windows)
    windows.findChild(QMenu, 'menuFile').menuAction().trigger()
    windows.findChild(QAction, 'actionCreate_a_file').trigger()
    item = windows.table_widget.findItems('test.txt', Qt.MatchContains)[0]
    windows.table_widget.item(item.row(), item.column()).setSelected(True)
    windows.findChild(QAction, 'actionOpen_by_default').trigger()
    windows.table_widget.item(item.row(), item.column()).setSelected(True)
    windows.findChild(QAction, 'actionDelete').trigger()


def test_copy_paste(qtbot):
    """Test copy paste of file / directory / shortcut on main windows"""
    windows = UiExpleurer()
    windows.show()
    qtbot.addWidget(windows)
    windows.findChild(QMenu, 'menuFile').menuAction().trigger()
    windows.findChild(QAction, 'actionCreate_a_file').trigger()
    item = windows.table_widget.findItems('test.txt', Qt.MatchContains)[0]
    windows.table_widget.item(item.row(), item.column()).setSelected(True)
    windows.findChild(QAction, 'actionCopy').trigger()
    windows.findChild(QAction, 'actionMove').trigger()
    item = windows.table_widget.findItems('test.txt', Qt.MatchContains)[0]
    windows.table_widget.item(item.row(), item.column()).setSelected(True)
    windows.findChild(QAction, 'actionDelete').trigger()

    windows.findChild(QMenu, 'menuFile').menuAction().trigger()
    windows.findChild(QAction, 'actionCreate_a_directory').trigger()
    item = windows.table_widget.findItems('test', Qt.MatchContains)[0]
    windows.table_widget.item(item.row(), item.column()).setSelected(True)
    windows.findChild(QAction, 'actionCopy').trigger()
    windows.findChild(QAction, 'actionMove').trigger()
    item = windows.table_widget.findItems('test', Qt.MatchContains)[0]
    windows.table_widget.item(item.row(), item.column()).setSelected(True)
    windows.findChild(QAction, 'actionDelete').trigger()

    windows.findChild(QMenu, 'menuFile').menuAction().trigger()
    windows.findChild(QAction, 'actionCreate_a_shortcut').trigger()
    item = windows.table_widget.findItems('test.lnk', Qt.MatchContains)[0]
    windows.table_widget.item(item.row(), item.column()).setSelected(True)
    windows.findChild(QAction, 'actionCopy').trigger()
    windows.findChild(QAction, 'actionMove').trigger()
    item = windows.table_widget.findItems('test.lnk', Qt.MatchContains)[0]
    windows.table_widget.item(item.row(), item.column()).setSelected(True)
    windows.findChild(QAction, 'actionDelete').trigger()


def test_cut_paste(qtbot):
    """Test cut paste of file / directory / shortcut on main windows"""
    windows = UiExpleurer()
    windows.show()
    qtbot.addWidget(windows)
    windows.findChild(QMenu, 'menuFile').menuAction().trigger()
    windows.findChild(QAction, 'actionCreate_a_file').trigger()
    item = windows.table_widget.findItems('test.txt', Qt.MatchContains)[0]
    windows.table_widget.item(item.row(), item.column()).setSelected(True)
    windows.findChild(QAction, 'actionCut').trigger()
    windows.findChild(QAction, 'actionMove').trigger()
    item = windows.table_widget.findItems('test.txt', Qt.MatchContains)[0]
    windows.table_widget.item(item.row(), item.column()).setSelected(True)
    windows.findChild(QAction, 'actionDelete').trigger()

    windows.findChild(QAction, 'actionCreate_a_directory').trigger()
    item = windows.table_widget.findItems('test', Qt.MatchContains)[0]
    windows.table_widget.item(item.row(), item.column()).setSelected(True)
    windows.findChild(QAction, 'actionCut').trigger()
    windows.findChild(QAction, 'actionMove').trigger()
    item = windows.table_widget.findItems('test', Qt.MatchContains)[0]
    windows.table_widget.item(item.row(), item.column()).setSelected(True)
    windows.findChild(QAction, 'actionDelete').trigger()

    windows.findChild(QAction, 'actionCreate_a_shortcut').trigger()
    item = windows.table_widget.findItems('test.lnk', Qt.MatchContains)[0]
    windows.table_widget.item(item.row(), item.column()).setSelected(True)
    windows.findChild(QAction, 'actionCut').trigger()
    windows.findChild(QAction, 'actionMove').trigger()
    item = windows.table_widget.findItems('test.lnk', Qt.MatchContains)[0]
    windows.table_widget.item(item.row(), item.column()).setSelected(True)
    windows.findChild(QAction, 'actionDelete').trigger()


def test_search(qtbot):
    """Test search file / directory / shortcut on main windows"""
    windows = UiExpleurer()
    windows.show()
    qtbot.addWidget(windows)
    windows.findChild(QAction, 'actionCreate_a_file').trigger()
    windows.findChild(QAction, 'actionSearch').trigger()
    item = windows.table_widget.findItems('test.txt', Qt.MatchContains)[0]
    windows.table_widget.item(item.row(), item.column()).setSelected(True)
    windows.findChild(QAction, 'actionDelete').trigger()
    item = windows.table_widget.findItems('test.txt', Qt.MatchContains)[0]
    windows.table_widget.item(item.row(), item.column()).setSelected(True)
    windows.findChild(QAction, 'actionRename').trigger()
    item = windows.table_widget.findItems('test.txt', Qt.MatchContains)[0]
    windows.table_widget.item(item.row(), item.column()).setSelected(True)
    windows.findChild(QAction, 'actionMove').trigger()

    windows.findChild(QMenu, 'menuSort').menuAction().trigger()
    windows.findChild(QAction, 'actionBy_name').trigger()
    windows.findChild(QAction, 'actionBy_type').trigger()
    windows.findChild(QAction, 'actionBy_extension').trigger()
    windows.findChild(QAction, 'actionBy_modification_date').trigger()

    windows.findChild(QMenu, 'menuDisplay').menuAction().trigger()
    windows.findChild(QAction, 'actionSimple').trigger()
    windows.findChild(QAction, 'actionIn_detail').trigger()

    windows.line_edit.setText(str(Path().home()))

    item = windows.table_widget.findItems('test.txt', Qt.MatchContains)[0]
    windows.table_widget.item(item.row(), item.column()).setSelected(True)
    windows.findChild(QAction, 'actionDelete').trigger()


def test_on_press_directory(qtbot):
    """Test on press key Enter / double Left click when a directory is focus on main windows"""
    windows = UiExpleurer()
    windows.show()
    qtbot.addWidget(windows)
    item = windows.table_widget.findItems('..', Qt.MatchContains)[0]
    windows.table_widget.item(item.row(), item.column()).setSelected(True)
    windows.table_widget.doubleClicked.emit(windows.table_widget.selectedIndexes()[0])

    item = windows.table_widget.findItems('..', Qt.MatchContains)[0]
    windows.table_widget.item(item.row(), item.column()).setSelected(True)
    qtbot.keyPress(item.tableWidget(), Qt.Key_Enter)


def test_on_press_shortcut_on_the_left(qtbot):
    """Test on double left click when a directory is focus on main windows"""
    windows = UiExpleurer()
    windows.show()
    qtbot.addWidget(windows)
    item = windows.tree_widget.topLevelItem(0)
    windows.tree_widget.itemPressed.emit(item.child(0), 0)
