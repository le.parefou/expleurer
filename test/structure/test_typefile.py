"""Test module according to src/structure/typefile"""
import unittest

from expleurer.structure.typefile import suffix_to_type


class TestTypeFile(unittest.TestCase):
    """Test function according to the typefile module"""

    def test_suffix_to_type(self) -> None:
        """
        Test the suffix_to_type function with all (supporting) extension file

        :return: Nothing
        """
        directory = "C:\\"
        self.assertEqual('Repertoire', suffix_to_type(directory))

        all_text_files_testing = ['test.py', 'test.sh', 'test.txt', 'test.js', 'test.csv', 'test.json', 'test.html',
                                  'test.css', 'test.numbers', 'test.doc', 'test.xlsx', 'test.odt', 'test.ppt',
                                  'test.pdf', 'test.pptx', 'test.docx', 'test.key', 'test.odp', 'test.pages',
                                  'test.xls', 'test.ods']
        for elt in all_text_files_testing:
            self.assertEqual('Texte', suffix_to_type(elt))

        all_audio_files_testing = ['test.mp3', 'test.flac', 'test.wav']
        for elt in all_audio_files_testing:
            self.assertEqual('Audio', suffix_to_type(elt))

        all_video_files_testing = ['test.avi', 'test.webm', 'test.mov', 'test.mp4']
        for elt in all_video_files_testing:
            self.assertEqual('Video', suffix_to_type(elt))

        all_images_files_testing = ['test.bmp', 'test.tiff', 'test.jpg', 'test.png', 'test.jpeg', 'test.gif']
        for elt in all_images_files_testing:
            self.assertEqual('Image', suffix_to_type(elt))

# if __name__ == '__main__':
#     unittest.main()
