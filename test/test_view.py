"""Test module according to src/view"""
import unittest

from faker import Faker
from faker.providers import file

from expleurer.view import *


class TestViewConcept(unittest.TestCase):
    """Test class according to ViewFeature"""

    def setUp(self) -> None:
        fake = Faker('fr_FR')
        fake.add_provider(file)
        Faker.seed(5)
        self.fake_data = [fake.file_name() for _ in range(20)]

    def test_sort_file_by_type(self):
        """
        Test sort files by type class method

        :return: Nothing
        """
        files_sorted_by_type = sort_by_type(self.fake_data)
        expected_list = ['gloire.mp3', 'naturel.mp3', 'éclater.flac', 'autre.tiff', 'dehors.gif', 'embrasser.bmp',
                         'famille.gif', 'genou.jpg', 'madame.bmp', 'soin.png', 'bon.csv', 'conscience.csv',
                         'devant.html', 'doute.pptx', 'marché.csv', 'devant.avi', 'lien.mov', 'pensée.mp4', 'rêver.mp4',
                         'se.mp4']
        self.assertEqual(expected_list, files_sorted_by_type)

    def test_sort_file_by_extension(self):
        """
        Test sort files by extension class method

        :return: Nothing
        """
        files_sorted_by_extension = sort_by_extension(self.fake_data)
        expected_list = ['devant.avi', 'embrasser.bmp', 'madame.bmp', 'bon.csv', 'conscience.csv', 'marché.csv',
                         'éclater.flac', 'dehors.gif', 'famille.gif', 'devant.html', 'genou.jpg',
                         'lien.mov', 'gloire.mp3', 'naturel.mp3', 'pensée.mp4', 'rêver.mp4', 'se.mp4', 'soin.png',
                         'doute.pptx', 'autre.tiff']
        self.assertEqual(expected_list, files_sorted_by_extension)

    def test_sort_file_by_name(self):
        """
        Test sort files by name class method

        :return: Nothing
        """
        file_sorted_by_name = sort_by_name(self.fake_data)
        expected_list = ['autre.tiff', 'bon.csv', 'conscience.csv', 'dehors.gif', 'devant.avi', 'devant.html',
                         'doute.pptx', 'embrasser.bmp', 'famille.gif', 'genou.jpg', 'gloire.mp3',
                         'lien.mov', 'madame.bmp', 'marché.csv', 'naturel.mp3', 'pensée.mp4', 'rêver.mp4',
                         'se.mp4', 'soin.png', 'éclater.flac']
        self.assertEqual(expected_list, file_sorted_by_name)

    def test_sort_file_by_date(self) -> None:
        """
        Test sort files by date class method

        :return: Nothing
        """
        real_data = [str(file_path) for file_path in Path("C:\\Users\\leparefou\\Documents\\real_data").iterdir()]
        file_sorted_by_name = sort_by_date(real_data)
        expected_result = ['C:\\Users\\leparefou\\Documents\\real_data\\.gitignore',
                           'C:\\Users\\leparefou\\Documents\\real_data\\README.md',
                           'C:\\Users\\leparefou\\Documents\\real_data\\requirements.txt',
                           'C:\\Users\\leparefou\\Documents\\real_data\\.coverage',
                           'C:\\Users\\leparefou\\Documents\\real_data\\.pylintrc',
                           'C:\\Users\\leparefou\\Documents\\real_data\\script.sh']
        self.assertEqual(expected_result, file_sorted_by_name)

    def test_search_a_file_or_a_directory(self) -> None:
        """
        Test search a file in path class method

        :return: Nothing
        """
        file_corresponding_to_search = search_in_path('pylint', 'C:\\Users\\leparefou\\Documents\\')
        expected_result = ['C:\\Users\\leparefou\\Documents\\expleurer\\.pylintrc',
                           'C:\\Users\\leparefou\\Documents\\expleurer\\venv\\Lib\\site-packages\\pylint',
                           'C:\\Users\\leparefou\\Documents\\expleurer\\venv\\Lib\\site-packages\\pylint-2.11.1.dist'
                           '-info',
                           'C:\\Users\\leparefou\\Documents\\expleurer\\venv\\Lib\\site-packages\\pylint\\epylint.py',
                           'C:\\Users\\leparefou\\Documents\\expleurer\\venv\\Lib\\site-packages\\pylint\\lint'
                           '\\pylinter.py',
                           'C:\\Users\\leparefou\\Documents\\expleurer\\venv\\Lib\\site-packages\\pylint\\lint'
                           '\\__pycache__\\pylinter.cpython-310.pyc',
                           'C:\\Users\\leparefou\\Documents\\expleurer\\venv\\Lib\\site-packages\\pylint\\__pycache__'
                           '\\epylint.cpython-310.pyc',
                           'C:\\Users\\leparefou\\Documents\\expleurer\\venv\\Scripts\\epylint.exe',
                           'C:\\Users\\leparefou\\Documents\\expleurer\\venv\\Scripts\\pylint.exe',
                           'C:\\Users\\leparefou\\Documents\\real_data\\.pylintrc']
        self.assertEqual(file_corresponding_to_search, expected_result)

if __name__ == '__main__':
    unittest.main()
