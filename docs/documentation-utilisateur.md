# Documentation utilisateur

## Objectif du document

Ce présent document permet de lister les différents cas d'usages offerts par le logiciel pour l'utilisateur final. Pour
aider l'utilisateur, ce document fournit les étapes à réaliser pour produire l'un des cas d'usages du logiciel.
L'utilisateur peut également être amenée à obtenir des informations sur la façon dont est présenté le logiciel afin de
le prendre en main plus facilement.

## Présentation du logiciel

Le logiciel **expleurer** est une alternative à l'explorateur de fichier présent sur l'environnement Microsoft Windows
sous le nom d'*explorer.exe* par défaut.
**expleurer** se veut simple d'utilisation, open-source et personnalisable.

Les fonctionnalités qui seront détaillées dans la suite de ce document sont l'essence même d'un explorateur de fichier
de base et sont découpés en deux types d'opérations :

- **les opérations de modification** : ce sont les fonctionnalités qui vont agir sur le gestionnaire de fichier. On y
  retrouve les fonctionnalités de création, renommage, suppression, copiage et déplacement.
- **les opérations de visualisation** : ce sont les fonctionnalités qui vont présenter les fichiers sur le logiciel. On
  y retrouve les fonctionnalités de tri, de recherche et d'affichage.

Lorsque l'utilisateur lance le logiciel, il arrive sur le dossier de l'utilisateur courant avec l'affichage suivant :

![Capture d'écran de l'application lors de son ouverture](default_screen.png "A l'ouverture de l'application")

1) **Les menus** : là où se trouve certaines fonctionnalités. Un clic gauche sur un des sous-menus permet d'interagir.
2) **L'arborescence de fichier** : présente quelques dossiers pour y accéder plus rapidement.
3) **La liste des fichiers** : affiche le contenu du dossier courant.

Dans les prochaines parties, l'utilisateur en apprendra plus sur comment utiliser le logiciel afin de réaliser certaines
tâches.

## La navigation dans le logiciel

Comme expliqué lors de la présentation du logiciel, l'**expleurer** affiche les dossiers de l'utilisateur courant
lorsqu'il est lancé.

Pour pouvoir naviguer dans l'**expleurer** plusieurs choix s'offrent à l'utilisateur :

- utiliser **l'arborescence de fichier** afin d'accéder plus rapidement au répertoire courant de l'utilisateur et des
  périphériques.
- réaliser un **double-clique gauche** avec la souris sur un répertoire présent dans la **liste des fichiers**.
- appuyer sur la touche **entrée** lorsqu'un répertoire est sélectionné pour le traverser.
- utiliser la barre de navigation en haut pour fournir le chemin du fichier.

## Opération de Modification

### Créer un fichier

L'utilisateur peut créer un fichier de la manière suivante :

- menu **Fichier > Créer un fichier**
- la combinaison de touche **CTRL + n**

Ces deux manières permettent d'appeler une boite de dialogue qui lui demandera de fournir le nom du fichier à créer.

### Créer un dossier

L'utilisateur peut créer un dossier de la manière suivante :

- menu **Fichier > Créer un dossier**

Une boite de dialogue s'affiche alors et demande à l'utilisateur le nom à fournir pour la création du dossier.

### Créer un raccourci

! TODO - revoir la fonction raccourcis

L'utilisateur peut créer un raccourci de la manière suivante :

- menu **Fichier > Créer un raccourci**

Une boite de dialogue apparait et demande à l'utilisateur de lui fournir deux informations :

- le **nom du raccourci**, c'est le nom qui sera donné au raccourci.
- la **cible du raccourci**, là où va pointer le raccourcis.

### Renommer un fichier, un dossier ou un raccourci

L'utilisateur peut renommer un fichier, dossier ou raccourci de la manière suivante :

- sélectionner l'élément à renommer, puis appuyer sur la touche F2
- réaliser un **clic-droit** sur l'élément et choisir l'option **Renommer**

### Supprimer un fichier

L'utilisateur peut supprimer un fichier, dossier ou raccourci de la manière suivante :

- sélectionner l'élément à supprimer, puis appuyer sur la touche **Suppr**.
- réaliser un **clic-droit** sur l'élément et choisir l'option **Supprimer**

### Déplacer un fichier, fichier ou raccourci

L'utilisateur peut déplacer un fichier, dossier ou raccourci de la manière suivante :

- sélectionner l'élément à déplacer, puis appuyer sur la combinaison de touche **CTRL + x**
- réaliser un **clic-droit** sur l'élément et choisir l'option **Couper**

L'élément est ensuite dans la mémoire tampon du logiciel. Pour pouvoir poursuivre, il faut suivre l'opération pour
déposer un fichier.

### Copier un fichier, dossier ou raccourci

L'utilisateur peut copier un fichier, dossier ou raccourci de la manière suivante :

- sélectionner l'élément à déplacer, puis appuyer sur la combinaison de touche **CTRL + c**
- réaliser un **clic-droit** sur l'élément et choisir l'option **Copier**

L'élément est ensuite dans la mémoire tampon du logiciel. Pour pouvoir poursuivre, il faut suivre l'opération pour
déposer un fichier.

### Déposer un fichier

L'utilisateur peut déposer un fichier, dossier ou raccourci à condition qu'une opération de copie ou de déplacement a
été faite préalablement. Si cette condition est remplie, il peut alors déposer son élément de la manière suivante :

- réaliser la combinaison de touche **CTRL + v** dans le dossier cible.
- réaliser un **clic-droit** dans le contenu du dossier cibler et choisir l'option **Déplacer**.

Dans le cas de la copie, le fichier à copier est gardé en mémoire dans le logiciel jusqu'à qu'une opération de copie ou
de déplacement se fasse. Il est donc possible de déposer le fichier préalablement copier à différents endroits en même
temps.

Dans le cas du déplacement, le fichier est déposé une seule fois. Il disparait de son chemin d'origine.

## Opération de visualisation

### Trier par type

L'utilisateur peut trier par type en allant dans le menu **Trier > Trier par type**.

Cela aura pour effet de trier les éléments du dossier courant par type.

### Trier par extension

L'utilisateur peut trier par extension de fichier en allant dans le menu **Trier > Trier par extension**.

Cela aura pour effet de trier les éléments du dossier courant par extension de fichier.

### Trier par nom

L'utilisateur peut trier par nom en allant dans le menu **Trier > Trier par nom**.

Cela aura pour effet de trier les éléments du dossier courant par nom.

### Trier par date de modification

L'utilisateur peut trier par date de modification en allant dans le menu **Trier > Trier par date de modification**.

Cela aura pour effet de trier les éléments du dossier courant par date de modification.

### Affichage simplifié (nom, icône, extension)

L'utilisateur peut afficher les éléments du dossier courant de manière simplifié en allant dans le menu **Afficher >
Simple**.

### Affichage détaillé (nom, icône, type, extension, date)

L'utilisateur peut afficher les éléments du dossier courant de manière détaillé en allant dans le menu **Afficher > En
détails**.

### Rechercher un fichier

L'utilisateur peut rechercher un fichier de la manière suivante :

- réaliser la combinaison de touche **CTRL + f** dans le dossier cible.
- cliquer sur le menu **Rechercher**.

Une boite de dialogue s'affiche, demandant le pattern à rechercher dans le dossier courant.