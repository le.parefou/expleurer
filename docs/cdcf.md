# Cahier des charges fonctionnelles

## Opération de Modification

- [x]  Créer un fichier
- [x]  Créer un dossier
- [x]  Créer un raccourci
- [x]  Renommer un fichier
- [x]  Renommer un dossier
- [x]  Renommer un raccourci
- [x]  Supprimer un fichier
- [x]  Supprimer un dossier
- [x]  Supprimer un raccourci
- [x]  Déplacer un fichier
- [x]  Déplacer un dossier
- [x]  Déplacer un raccourci
- [x]  Copier un fichier
- [x]  Copier un dossier
- [x]  Copier un raccourci

## Opération de visualisation

D'abord en cli, puis en GUI (pyqt5)

- [x]  Trier par type
- [x]  Trier par extension
- [x]  Trier par nom
- [x]  Trier par date
- [x]  Affichage simplifié (nom, icone)
- [x]  Affichage détaillé (nom, icone, type, extension, date)
- [x]  Rechercher un fichier
- [x]  Naviguer à travers l'arborescence de fichier 