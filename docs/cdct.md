# Cahier des charges techniques

## Langages de programmation

- python

# Modules externes

- Système
    - [x] pathlib --> chemin de fichier
    - [x] shutil (rmtree, copytree) --> supprimer / copier récursivement
    - [x] winshell (shortcut) - pywin32 est requis --> créer un link sur windows
    - [x] os --> ouvrir l'application par défaut
    - [x] stat --> analyser les permissions du fichier
    - [x] psutil --> pour récupérer les partitions

- Graphique (UI - User Interface)
    - ~~[x] pyqt5 (librairie QT)~~ 
    - [x] pyside6 (librairie QT)

- Test
    - [x] unittest
    - [x] faker --> fausse les données de fichier
    - [x] qtbot --> simule l'intéraction avec l'interface graphique