# Cahier des charges graphiques

## Chartes graphiques

### Police de caractères
- font
- taille
- couleur
- mise en valeur

### Couleurs
- clair
- sombre
- foreground
- background

## Composants

### Menu

### Action

### Tableau

### Cellule

### Arborescence

### Dialog

### Zone de texte

### Menu contextuel

